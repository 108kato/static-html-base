export function getInfo() {
    var filePath = "https://script.googleusercontent.com/a/macros/108.works/echo?user_content_key=t-5DmqPatk_CH0lZcNeF6S85ksySCOKrHNVPeOOiNHKvHlbgJ56VjLpmqNQ7crXMKfgUWoMIW9Our2fgT9Ie_qslZ6N1Ozc-m5_BxDlH2jW0nuo2oDemN9CCS2h10ox_nRPgeZU6HP8ImY7yg-ahIfEDBKCpnwQV64r6twEbafRYGFLjmxBXt_1qo0OPWVlAJww11HuNpJ1taJ4sFJEUqXIfCB7rwP5tbxVvqvSKX_-agQEneoX2wQ&lib=M5qWGGDhTYg1V00FEOITUksZwjq1zALPm";

    $.getJSON(filePath, function (data) {
        var txt = data[0]["INFORMATION"];
        $("#js-information p").text(escape_html(txt));
    });
    function escape_html(string) {
        if (typeof string !== 'string') {
            return string;
        }
        return string.replace(/[&'`"<>]/g, function (match) {
            return {
                '&': '&amp;',
                "'": '&#x27;',
                '`': '&#x60;',
                '"': '&quot;',
                '<': '&lt;',
                '>': '&gt;',
            } [match]
        });
    }
}