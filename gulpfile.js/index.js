"use strict";
// const gulp = require("gulp");
const { gulp, series, parallel, watch } = require('gulp');
// const del = require("del");
const changed = require('gulp-changed');
const { browserSyncInit, browserSyncReload } = require("./browsersync");
const { css } = require("./sass");
const { html } = require("./ejs");
// const { images } = require("./images");
const { webpackScript } = require("./webpack_script");

// exports.css = css;
// exports.html = html;
// exports.webpackScript = webpackScript;


// Clean assets
function clean(cb) {
    return del(["./dist/assets/img/"]);
    cb();
}

function watchFiles(cb) {
    watch("_src/js/**/*.js", series(webpackScript, browserSyncReload));
    watch("_src/scss/**/*.scss", series(css, browserSyncReload));
    watch("_src/ejs/**/*.ejs", series(html, browserSyncReload));
    // watch("_src/ejs/assets/img/**/*", series(images, browserSyncReload));
    cb();
}

exports.watchFiles = watchFiles;
exports.default = series(parallel(webpackScript, css, html), watchFiles, browserSyncInit);