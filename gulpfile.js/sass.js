"use strict";
const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('autoprefixer');
const changed = require('gulp-changed');
const plumber = require('gulp-plumber');
const rename = require("gulp-rename");
const postcss = require("gulp-postcss");
const cssnano = require("cssnano");

const path = require('path');
const fs = require('fs');
const pkg = JSON.parse(fs.readFileSync('./package.json'));
const assetsPath = path.resolve(pkg.path.assetsDir);

function css() {
    console.log("css");
    return gulp
        .src("./_src/scss/**/*.scss")
        .pipe(plumber({
            errorHandler: function (err) {
                console.log(err.messageFormatted);
                this.emit('end');
            }
        }))
        .pipe(sass({ outputStyle: "expanded" }))
        .pipe(gulp.dest(assetsPath + "/css/"))
        .pipe(rename({ suffix: ".min" }))
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(gulp.dest(assetsPath + "/css/"));
}
exports.css = css;