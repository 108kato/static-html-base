"use strict";
const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const newer = require("gulp-newer");

// Optimize Images
function images() {
    return gulp
        .src("./_src/ejs/assets/img/**/*")
        .pipe(newer("./_src/ejs/assets/img/**/*"))
        .pipe(
            imagemin([
                imagemin.gifsicle({ interlaced: true }),
                imagemin.mozjpeg({ progressive: true }),
                imagemin.optipng({ optimizationLevel: 5 }),
                imagemin.svgo({
                    plugins: [
                        {
                            removeViewBox: false,
                            collapseGroups: true
                        }
                    ]
                })
            ])
        )
        .pipe(gulp.dest("./dist/assets/img"));
}
exports.images = images;